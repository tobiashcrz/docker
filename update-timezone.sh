#!/bin/bash

echo "----Populating variables----" 
ianaUrl=https://github.com/bell-sw/IANAUpdater/releases/download/1.0/IANAUpdater.jar 
tzdburl=https://data.iana.org/time-zones/tzdb-latest.tar.lz 
ianaUpdater=$(basename ${ianaUrl}) 
tzdbfilename=$(basename $tzdburl .tar.lz) 

echo "----Adding dependencies----" 
apk add lzip 
apk add make 

echo "----Getting tzdb----" 
wget $tzdburl 
lzip -d ${tzdbfilename}.tar.lz 
tar xvf ${tzdbfilename}.tar 
directory=$(find . -type d -maxdepth 1 -name 'tzdb*' | cut -f2 -d"/") 
cd ${directory}/ 

echo "----Making rearguard----" 
make rearguard_tarballs 
tzdbrearfile=$(find . -type f -name "*.tar.gz") 

echo "----Executing IANAUpdater----" 
wget ${ianaUrl} 
java -jar ${ianaUpdater} -z file://$(pwd)/${tzdbrearfile} -t ${JAVA_HOME} 

echo "----Cleaning----" 
cd .. 
rm -rf ${directory}/ 
rm ${tzdbfilename}.tar